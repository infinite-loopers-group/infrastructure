terraform {
  backend "s3" {
    bucket = "infinite-loopers-bd"
    key    = "preprod/monitoring/terraform.tfstate"
    region = "eu-west-3"
    encrypt = true
  }
}