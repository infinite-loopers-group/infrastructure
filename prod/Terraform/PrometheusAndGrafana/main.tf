resource "aws_security_group" "prometheus-grafana-prod-sg" {
  name        = "myprometheus_grafana_allow_ports"
  description = "Allow port(s) inbound traffic and all outbound traffic"
  # vpc_id      = "vpc-0e6cf1195c50980f8"
  vpc_id      = data.aws_vpcs.myvpcs.ids[0]

  tags = {
    Name = "prometheus-grafana-prod-sg"
  }
}

resource "aws_vpc_security_group_ingress_rule" "allow_promotheus_9090" {
  security_group_id = aws_security_group.prometheus-grafana-prod-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 0
  ip_protocol       = "tcp"
  to_port           = 9090
}

resource "aws_vpc_security_group_ingress_rule" "allow_grafana_3000" {
  security_group_id = aws_security_group.prometheus-grafana-prod-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 0
  ip_protocol       = "tcp"
  to_port           = 3000
}


resource "aws_vpc_security_group_ingress_rule" "allow_icmp" {
  security_group_id = aws_security_group.prometheus-grafana-prod-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = -1
  ip_protocol       = "icmp"
  to_port           = -1
}

resource "aws_vpc_security_group_egress_rule" "allow_all_traffic_ipv4" {
  security_group_id = aws_security_group.prometheus-grafana-prod-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1" # semantically equivalent to all ports
}

resource "aws_instance" "prometheus-grafana-prod" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  subnet_id     = "subnet-018dd2a28bac3ca43"
  vpc_security_group_ids = [ aws_security_group.prometheus-grafana-prod-sg.id ]
  key_name      = "terraform-aws"

  user_data     = "${file("install_prometheus_and_grafana.sh")}"

  tags = {
    Name = "prometheus-grafana-prod"
  }
}