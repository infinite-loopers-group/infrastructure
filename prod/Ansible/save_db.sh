#!/bin/bash

# create a backup with mongdump (connect to the localhost with default port)
cd $1
dirOut=`date '+%d%m-%Hh%M'`"_morningnews"
# mongodump --uri mongodb+srv://admincapsule:capsule2024@lacapsule1.naow7jb.mongodb.net/morningnews --out=`date '+%d%m-%Hh%M'`"_morningnews"
# mongodump --uri mongodb://admincapsule:capsule2024@mongo.lacapsuleproject.online -d morningnews --out=$dirOut
mongodump --uri mongodb://admincapsule:capsule2024@localhost -d morningnews --out=$dirOut

# test if s3 already exists
# if not, create it
# And copy recursively local backup to remote s3 bucket
aws s3 ls s3://morningnews-prod
! test $? == 0 && aws s3 mb s3://morningnews-prod
aws s3 cp $dirOut s3://morningnews-prod/$dirOut --recursive