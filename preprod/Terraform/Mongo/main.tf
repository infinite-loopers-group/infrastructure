resource "aws_key_pair" "terraform-aws" {
  key_name   = "terraform-aws"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAGWCps1Q5WxV1uE5Dlz5kNJXNmRvv8GLRn/eX9GwzhRQ+2FLJzo2U0/C/rHZHkZQPfkkF1RjPCcyUKRq96wK2NWlzULLXE9C4Xb0Ymm42f2rLgkeGNTiMjrzb8wDeLmHqCEQhRZPEqT1Kjlb+SWrqYlNaYJBPtPTe9uic2dDk1u/rQa9QU8Ud3YDlptv5Nk1e3hve1XH7vjmOY9t31QX6ARzYjAqHhl6QGKCCB7GrNSFqPxvmDkq/6MVljNDnkayA8xoIRv/pYB3MYDAwK5IR6VbQx0Z+dx0asfD0HM2ZIQkFLM1fUo/tdwS72huzRVzUTDjvpbXGeeLRvFUsYlqrIh/nGEeGMI29oP1nVmC0sP+YdJyrFa7oJHOx0vhDaW4x7T5GtUdO++voySCAq0Zx9i4SLfyEfo0jRc3SSEE6RT76OlzRXoObI3xjA9+KaHObyziqd2So7oLmUZMOSTyplBpPzU6qU7HmJuFJVpenopNkdK+/wjDB7h9aX5Wrixc= olivier@olivier-victus"
}

resource "aws_security_group" "mongo-sg" {
  name        = "myvm_allow_ssh_appl_port"
  description = "Allow SSH and application port(s) inbound traffic and all outbound traffic"
  vpc_id      = data.aws_vpcs.myvpcs.ids[0]

  tags = {
    Name = "mongo-sg"
  }
}

resource "aws_vpc_security_group_ingress_rule" "allow_ssh" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 0
  ip_protocol       = "tcp"
  to_port           = 22
}

resource "aws_vpc_security_group_ingress_rule" "allow_new_ssh" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 0
  ip_protocol       = "tcp"
  to_port           = 2223
}


resource "aws_vpc_security_group_ingress_rule" "allow_mongo" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 0
  ip_protocol       = "tcp"
  to_port           = 27017
}


resource "aws_vpc_security_group_ingress_rule" "allow_icmp" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = -1
  ip_protocol       = "icmp"
  to_port           = -1
}

resource "aws_vpc_security_group_egress_rule" "allow_all_traffic_ipv4" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1" # semantically equivalent to all ports
}

resource "aws_instance" "mongo-vm" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  subnet_id     = "subnet-01f645f982a561d88"
  vpc_security_group_ids = [ aws_security_group.mongo-sg.id ]
  key_name      = "terraform-aws"

  user_data     = "${file("install_docker.sh")}"

  tags = {
    Name = "mongo"
  }
}