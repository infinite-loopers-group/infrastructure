terraform {
  backend "s3" {
    bucket = "infinite-loopers-bd2"
    key    = "preprod/morningnews/terraform.tfstate"
    region = "eu-west-3"
    encrypt = true
  }
}