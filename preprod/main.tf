terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-west-3"
}

resource "aws_security_group" "application_security_group" {
  name = "morningnews_security"

  # Règle d'ingress pour SSH (port 2223)
  ingress {
    from_port   = 2223
    to_port     = 2223
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Règle d'ingress pour HTTP (port 80)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Règle d'ingress pour HTTPS (port 443)
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Règle d'ingress pour le port 9090 (Prometheus)
  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Règle d'ingress pour le port 8300 (Grafana)
  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Règle d'égress pour autoriser tout le trafic sortant
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



resource "aws_instance" "preprod_server" {
  ami                    = "ami-087da76081e7685da"
  instance_type          = "t2.micro"
  key_name               = "terraform-aws"
  vpc_security_group_ids = ["morningnews_security"]
  tags = {
    Name = "preprod_server"
  }
}
#création du fichier inventaire qui sera utilisé par ansible
output "public_ip" {
  value = aws_instance.preprod_server.public_ip
}

resource "local_file" "instance_ip_file" {
  filename = "inventaire"
  content  = aws_instance.preprod_server.public_ip
}