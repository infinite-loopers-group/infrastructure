terraform {
  backend "s3" {
    bucket = "infinite-loopers-bd"
    key    = "prod/morningnews/terraform.tfstate"
    region = "eu-west-3"
    encrypt = true
  }
}