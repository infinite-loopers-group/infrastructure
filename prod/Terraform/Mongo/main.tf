
resource "aws_security_group" "mongo-sg" {
  name        = "myvm_allow_ssh_appl_port"
  description = "Allow SSH and application port(s) inbound traffic and all outbound traffic"
  # vpc_id      = data.aws_vpcs.myvpcs.ids[0]
  vpc_id      = "vpc-087753ff2aa658256"

  tags = {
    Name = "mongo-sg"
  }
}

resource "aws_vpc_security_group_ingress_rule" "allow_ssh" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 22
  ip_protocol       = "tcp"
  to_port           = 22
}

resource "aws_vpc_security_group_ingress_rule" "allow_new_ssh" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 2223
  ip_protocol       = "tcp"
  to_port           = 2223
}

resource "aws_vpc_security_group_ingress_rule" "allow_mongo_exporter" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 9216
  ip_protocol       = "tcp"
  to_port           = 9216
}


resource "aws_vpc_security_group_ingress_rule" "allow_mongo" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 27015
  ip_protocol       = "tcp"
  to_port           = 27017
}


resource "aws_vpc_security_group_ingress_rule" "allow_icmp" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = -1
  ip_protocol       = "icmp"
  to_port           = -1
}

resource "aws_vpc_security_group_egress_rule" "allow_all_traffic_ipv4" {
  security_group_id = aws_security_group.mongo-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1" # semantically equivalent to all ports
}

resource "aws_instance" "mongo-vm" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  # subnet_id     = "subnet-01f645f982a561d88"
  subnet_id     = "subnet-02b29900d9b976af5"
  vpc_security_group_ids = [ aws_security_group.mongo-sg.id ]
  key_name      = "terraform-aws"

  user_data     = "${file("install_docker.sh")}"

  tags = {
    Name = "mongo"
  }
}