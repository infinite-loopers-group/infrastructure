#!/bin/bash

site="morningnews.lacapsuleproject.online"

# install prometheus and node exporter
sudo apt-get update
sudo apt install -y prometheus prometheus-node-exporter
sudo systemctl is-enabled prometheus
sudo systemctl status prometheus
sudo systemctl is-enabled prometheus-node-exporter
sudo systemctl status prometheus-node-exporter

# Append node exporter conf for nginx, remote node, and cadvisor
sudo sh -c 'printf "\n  - job_name: remote_node\n    static_configs:\n      - targets: ['$site:9100']\n\n  - job_name: nginx_exporter\n    static_configs:\n      - targets: ['$site:9113']\n\n  - job_name: mongodb_exporter\n    static_configs:\n      - targets: ['$site:9216']\n\n  - job_name: cadvisor\n    static_configs:\n      - targets: ['$site:8080']\n" >> /etc/prometheus/prometheus.yml'
sudo sed -i -e "s/$site:9100/'$site:9100'/g" -e "s/$site:9113/'$site:9113'/g" -e "s/$site:9216/'$site:9216'/g" -e "s/$site:8080/'$site:8080'/g" /etc/prometheus/prometheus.yml
sudo systemctl restart prometheus
sleep 5


# Install grafana OSS (free)
sudo apt-get install -y apt-transport-https software-properties-common wget
sudo mkdir -p /etc/apt/keyrings/
wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
# Updates the list of available packages
sudo apt-get update

sudo apt-get install -y grafana
sleep 3
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable grafana-server
sudo /bin/systemctl start grafana-server