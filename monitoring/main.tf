terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-west-3"
}




resource "aws_instance" "monitoring_server" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  key_name      = "terraform-aws"
  vpc_security_group_ids = ["web-server"]
  tags = {
    Name = "monitoring_server"
  }
}

output "public_ip" {
  value = aws_instance.monitoring_server.public_ip
}

resource "local_file" "instance_ip_file" {
  filename = "instance_ip.txt"
  content  = aws_instance.monitoring_server.public_ip
}